var express = require('express');
const Prometheus = require('prom-client');
var uuid = require('node-uuid');

var app = express();
var pg = require('pg');
var conString = process.env.POSTGRES_URL; // "postgres://username:password@localhost/database";

// custom metrics
const visitorTotal = new Prometheus.Counter({
  name: 'visitor_total',
  help: 'Total number of visitor',
  labelNames: ['count']
});

const httpRequestDurationMicroseconds = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500] // buckets for response time from 0.1ms to 500ms
});

// Runs before each requests, to find responseTime
app.use((req, res, next) => {
  res.locals.startEpoch = Date.now();
  // console.log("1. start", res.locals.startEpoch);
  next();
});

// Routes
app.get('/api/status', function (req, res, next) {
  visitorTotal.inc();
  // console.log("2. total:",visitorTotal);
  pg.connect(conString, function (err, client, done) {
    if (err) {
      res.status(500).send('error fetching client from pool');
      next(err);
    }
    client.query('SELECT now() as time', [], function (err, result) {
      // call `done()` to release the client back to the pool
      done();
      if (err) {
        res.status(500).send('error running query');
        next(err);
      }
      res.status(200).json({
        request_uuid: uuid.v4(),
        time: result.rows[0].time,
        visitor: visitorTotal
      });
    });
  });
  next();
});

// healthcheck, to route traffic to healty hosts
app.get('/healthz', function (req, res, next) {
  res.json({
    status: 'still alive ;)'
  });
  next();
});

app.get('/error', (req, res, next) => {
  next(new Error('Something went wrong :('));
});

app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType);
  res.end(Prometheus.register.metrics());
});

// // catch 404 and forward to error handler
// app.use((req, res, next) => {
//   console.log("inside 404");
//   var err = new Error('Not Found');
//   res.status(404);
//   res.json({ error: err.message });
// });

// Error handler
app.use((err, req, res, next) => {
  console.log('ERROR: Sending error logs to Honeybadger...(mock)');
  res.status(err.status || 500);
  // Do not expose your error in production
  res.json({ error: err.message });
  next();
});

// Runs after each requests, caluclate and capture respose time
app.use((req, res, next) => {
  const responseTimeInMs = Date.now() - res.locals.startEpoch;
  console.log('Elapsed responseTimeInMs: ', responseTimeInMs);
  httpRequestDurationMicroseconds
    .labels(req.method, req.route.path, res.statusCode)
    .observe(responseTimeInMs);
});

module.exports = app;
