FROM node:alpine

RUN apk update \
    && apk add --no-cache --virtual .ops-deps \
    curl busybox-extras \
    postgresql-client

RUN adduser deploy -D \
    && mkdir -p /home/deploy/app/node_modules \
    && chown -R deploy /home/deploy/app/node_modules
USER deploy

WORKDIR /home/deploy/app
COPY package.* /home/deploy/app
RUN npm install

COPY . /home/deploy/app

RUN npm install
ENTRYPOINT ["npm"]
CMD ["start"]
