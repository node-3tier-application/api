# Devops API App


### install the node packages for the api tier:
```sh
→ npm install
```

### start the app
```sh
→ npm start
```

###  NOTE this app uses two env variables:

- PORT: the listening PORT
- POSTGRES_URL: the postgresql url to connect

These two variables need to be set
